package GET;

import org.junit.jupiter.api.Test;
import org.testng.Assert;

import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.*;


class ListUsers {

	@Test
	void listUser() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		Response response = httpRequest.request(Method.GET, "/api/users?page=2");
		
		int statusCode = response.getStatusCode();
		String statusLine = response.getStatusLine();
		int responsePage = response.jsonPath().get("page");
		int responsePerPage = response.jsonPath().get("per_page");
		int responseTotal = response.jsonPath().get("total");
		int responseTotalPages = response.jsonPath().get("total_pages");
		
		//Tests
		Assert.assertEquals(statusCode, 200);
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
		Assert.assertEquals(responsePage, 2);
		Assert.assertEquals(responsePerPage, 3);
		Assert.assertEquals(responseTotal, 12);
		Assert.assertEquals(responseTotalPages, 4);
	}
	
	@Test
	void listSingleUser() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		Response response = httpRequest.request(Method.GET, "/api/users/2");
		
		int statusCode = response.getStatusCode();
		String statusLine = response.getStatusLine();
		
		Assert.assertEquals(statusCode, 200);
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
	}
	
	@Test
	void listUsersResource() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		Response response = httpRequest.request(Method.GET, "/api/unknown");
		
		int statusCode = response.getStatusCode();
		String statusLine = response.getStatusLine();
		int responsePage = response.jsonPath().get("page");
		int responsePerPage = response.jsonPath().get("per_page");
		int responseTotal = response.jsonPath().get("total");
		int responseTotalPages = response.jsonPath().get("total_pages");
		
		//Tests
		Assert.assertEquals(statusCode, 200);
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
		Assert.assertEquals(responsePage, 1);
		Assert.assertEquals(responsePerPage, 3);
		Assert.assertEquals(responseTotal, 12);
		Assert.assertEquals(responseTotalPages, 4);
	}
	
	@Test
	void listSingleUserResource() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		Response response = httpRequest.request(Method.GET, "/api/unknown/2");
		
		int statusCode = response.getStatusCode();
		String statusLine = response.getStatusLine();
		
		Assert.assertEquals(statusCode, 200);
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
	}
	
	@Test
	void userNotFound() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		Response response = httpRequest.request(Method.GET, "/api/users/23");
		
		int statusCode = response.getStatusCode();
		String responseBody = response.getBody().asString();
		
		//Tests
		Assert.assertEquals(statusCode, 404);
		Assert.assertEquals(responseBody, "{}");
	}
	
	@Test
	void userNotFoundResource() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		Response response = httpRequest.request(Method.GET, "/api/unknown/23");
		
		int statusCode = response.getStatusCode();
		String responseBody = response.getBody().asString();
		
		//Tests
		Assert.assertEquals(statusCode, 404);
		Assert.assertEquals(responseBody, "{}");
	}
	
	@Test
	void delayedResponse() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		Response response = httpRequest.request(Method.GET, "/api/users?delay=3");
		
		int statusCode = response.getStatusCode();
		long responseTime = response.getTime();
		boolean perform = responseTime < 4000;
		
		//Tests
		Assert.assertEquals(statusCode,200);
		Assert.assertTrue(perform);
	}

}
