package DELETE;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.testng.Assert;

import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

class DeleteUser {

	@Test
	void deleteUser() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		Response response = httpRequest.request(Method.DELETE, "api/users/2");
		
		int statusCode = response.getStatusCode();
		String responseBody = response.getBody().asString();
		
		//Tests
		Assert.assertEquals(statusCode, 204);
		Assert.assertEquals(responseBody.length(),0);
	}

}
