package POST;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

class LoginUser {

	@Test
	void loginSuccessful() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		
		//Create body to request
		JSONObject requestParams = new JSONObject();
		requestParams.put("email", "eve.holt@reqres.in");
		requestParams.put("password", "cityslicka");
		
		
		//Attach data to the request
		httpRequest.header("Content-Type","application/json");
		httpRequest.body(requestParams.toJSONString());
		
		//Response object
		Response response = httpRequest.request(Method.POST, "/api/login");
		
		int statusCode = response.getStatusCode();
		String responseToken = response.jsonPath().get("token");
		
		//Tests
		Assert.assertEquals(statusCode, 200);
		Assert.assertEquals(responseToken, "QpwL5tke4Pnpja7X4");
	}
	
	@Test
	void loginUnSuccessful() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		
		//Create body to request
		JSONObject requestParams = new JSONObject();
		requestParams.put("email", "peter@klaven");
		
		
		//Attach data to the request
		httpRequest.header("Content-Type","application/json");
		httpRequest.body(requestParams.toJSONString());
		
		//Response object
		Response response = httpRequest.request(Method.POST, "/api/login");
		
		int statusCode = response.getStatusCode();
		String responseError = response.jsonPath().get("error");
		
		//Tests
		Assert.assertEquals(statusCode, 400);
		Assert.assertEquals(responseError, "Missing password");
	}

}
