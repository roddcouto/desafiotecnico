package POST;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

class CreateUser {

	@Test
	void createUsers() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		
		//Create body to request
		JSONObject requestParams = new JSONObject();
		requestParams.put("name", "morpheus");
		requestParams.put("job", "leader");
		
		
		//Attach data to the request
		httpRequest.header("Content-Type","application/json");
		httpRequest.body(requestParams.toJSONString());
		
		//Response object
		Response response = httpRequest.request(Method.POST, "/api/users");
		
		int statusCode = response.getStatusCode();
		String contentType = response.header("Content-Type");
		String responseName = response.jsonPath().get("name");
		String responseJob = response.jsonPath().get("job");
		String responseCreatedAt = response.jsonPath().get("createdAt");
		
		//Tests
		Assert.assertEquals(statusCode, 201);
		Assert.assertEquals(contentType, "application/json; charset=utf-8");
		Assert.assertEquals(responseName, "morpheus");
		Assert.assertEquals(responseJob, "leader");
		Assert.assertNotNull(responseCreatedAt);
	}

}
