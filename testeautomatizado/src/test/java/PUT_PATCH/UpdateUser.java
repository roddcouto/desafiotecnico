package PUT_PATCH;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;
import org.testng.Assert;

import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

class UpdateUser {

	@Test
	void UpdateUserPut() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		
		//Create body to request
		JSONObject requestParams = new JSONObject();
		requestParams.put("name", "morpheus");
		requestParams.put("job", "zion resident");
		
		
		//Attach data to the request
		httpRequest.header("Content-Type","application/json");
		httpRequest.body(requestParams.toJSONString());
		
		//Response object
		Response response = httpRequest.request(Method.PUT, "/api/users/2");
		
		int statusCode = response.getStatusCode();
		String responseName = response.jsonPath().get("name");
		String responseJob = response.jsonPath().get("job");
		String responseUpdatedAt = response.jsonPath().get("updatedAt");
		
		//Tests
		Assert.assertEquals(statusCode, 200);
		Assert.assertEquals(responseName, "morpheus");
		Assert.assertEquals(responseJob, "zion resident");
		Assert.assertNotNull(responseUpdatedAt);
	}
	
	@Test
	void UpdateUserPatch() {
		baseURI = "https://reqres.in";
		RequestSpecification httpRequest = given();
		
		//Create body to request
		JSONObject requestParams = new JSONObject();
		requestParams.put("name", "morpheus");
		requestParams.put("job", "zion resident");
		
		
		//Attach data to the request
		httpRequest.header("Content-Type","application/json");
		httpRequest.body(requestParams.toJSONString());
		
		//Response object
		Response response = httpRequest.request(Method.PATCH, "/api/users/2");
		
		int statusCode = response.getStatusCode();
		String responseName = response.jsonPath().get("name");
		String responseJob = response.jsonPath().get("job");
		String responseUpdatedAt = response.jsonPath().get("updatedAt");
		
		//Tests
		Assert.assertEquals(statusCode, 200);
		Assert.assertEquals(responseName, "morpheus");
		Assert.assertEquals(responseJob, "zion resident");
		Assert.assertNotNull(responseUpdatedAt);
	}

}
